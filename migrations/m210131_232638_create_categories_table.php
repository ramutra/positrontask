<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%categories}}`.
 */
class m210131_232638_create_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%categories}}', [
            'id' => $this->primaryKey(),
            'name' => $this->text()->notNull(),
            'parent_id' => $this->integer(),
        ]);
        $this->createIndex('idx-categories-parent_id', '{{%categories}}', 'parent_id');
        $this->addForeignKey('fk-categories-parent', '{{%categories}}', 'parent_id', '{{%categories}}', 'id', 'SET NULL', 'RESTRICT');

        $this->createTable('{{%book_category}}', [
            'book_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull()
        ]);

        $this->addPrimaryKey('pk-book_category', '{{%book_category}}', ['book_id', 'category_id']);

        $this->createIndex('idx-book_category-book_id', '{{%book_category}}', 'book_id');
        $this->createIndex('idx-book_category-category_id', '{{%book_category}}', 'category_id');

        $this->addForeignKey('fk-book_category-book_id', '{{%book_category}}', 'book_id', '{{%books}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('fk-book_category-category_id', '{{%book_category}}', 'category_id', '{{%categories}}', 'id', 'CASCADE', 'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%book_category}}');
        $this->dropTable('{{%categories}}');
    }
}
