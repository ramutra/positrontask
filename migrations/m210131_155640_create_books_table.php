<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%books}}`.
 */
class m210131_155640_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%books}}', [
            'id' => $this->primaryKey(),
            'title' => $this->text()->notNull(),
            'article' => $this->string(),
            'page_count' => $this->integer(),
            'pub_date' => $this->date(),
            'pic_url' => $this->text()->notNull(),
            'short_desc' => $this->text(),
            'long_desc' => $this->text(),
            'status' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%books}}');
    }
}
