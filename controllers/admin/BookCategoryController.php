<?php

namespace app\controllers\admin;

use Yii;
use app\models\BookCategory;
use app\models\admin\search\BookCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BookCategoryController implements the CRUD actions for BookCategory model.
 */
class BookCategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BookCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BookCategory model.
     * @param integer $book_id
     * @param integer $category_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($book_id, $category_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($book_id, $category_id),
        ]);
    }

    /**
     * Creates a new BookCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($book_id = null)
    {
        $model = new BookCategory();
        $model->book_id = $book_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'book_id' => $model->book_id, 'category_id' => $model->category_id]);
            return $this->redirect(['admin/book/view', 'id' => $model->book_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BookCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $book_id
     * @param integer $category_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($book_id, $category_id)
    {
        $model = $this->findModel($book_id, $category_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'book_id' => $model->book_id, 'category_id' => $model->category_id]);
            return $this->redirect(['admin/book/view', 'id' => $model->book_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BookCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $book_id
     * @param integer $category_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($book_id, $category_id)
    {
        $this->findModel($book_id, $category_id)->delete();
        return $this->redirect(['admin/book/view', 'id' => $book_id]);
        // return $this->redirect(['index']);
    }

    /**
     * Finds the BookCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $book_id
     * @param integer $category_id
     * @return BookCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($book_id, $category_id)
    {
        if (($model = BookCategory::findOne(['book_id' => $book_id, 'category_id' => $category_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
