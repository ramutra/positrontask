<?php

namespace app\controllers\admin;

use Yii;
use app\models\BookAuthor;
use app\models\admin\search\BookAuthorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BookAuthorController implements the CRUD actions for BookAuthor model.
 */
class BookAuthorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BookAuthor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BookAuthorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BookAuthor model.
     * @param integer $book_id
     * @param integer $author_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($book_id, $author_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($book_id, $author_id),
        ]);
    }

    /**
     * Creates a new BookAuthor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($book_id = null)
    {
        $model = new BookAuthor();
        $model->book_id = $book_id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'book_id' => $model->book_id, 'author_id' => $model->author_id]);
            return $this->redirect(['admin/book/view', 'id' => $model->book_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BookAuthor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $book_id
     * @param integer $author_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($book_id, $author_id)
    {
        $model = $this->findModel($book_id, $author_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // return $this->redirect(['view', 'book_id' => $model->book_id, 'author_id' => $model->author_id]);
            return $this->redirect(['admin/book/view', 'id' => $model->book_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BookAuthor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $book_id
     * @param integer $author_id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($book_id, $author_id)
    {
        $this->findModel($book_id, $author_id)->delete();
        return $this->redirect(['admin/book/view', 'id' => $book_id]);

        // return $this->redirect(['index']);
    }

    /**
     * Finds the BookAuthor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $book_id
     * @param integer $author_id
     * @return BookAuthor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($book_id, $author_id)
    {
        if (($model = BookAuthor::findOne(['book_id' => $book_id, 'author_id' => $author_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
