<?php

namespace app\controllers;

use app\models\admin\search\CategorySearch;
use app\models\Book;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CatalogController extends Controller
{
    public function actionIndex()
    {
        $categoriesSearchModel = new CategorySearch();
        $categoriesDataProvider = $categoriesSearchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'categoriesSearchModel' => $categoriesSearchModel,
            'categoriesDataProvider' => $categoriesDataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
}
