<?php

use app\models\Book;
use app\models\BookAuthor;
use app\models\BookCategory;
use app\models\Category;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\admin\search\BookCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Book Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Book Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'book_id',
            [
                'label' => 'Book Title',
                'attribute' => 'book_id',
                'filter' => Book::find()->select(['title', 'id'])->indexBy('id')->column(),
                // 'value' => function (BookCategory $bookCategory) {
                //     return ArrayHelper::getValue($bookCategory, 'book.title');
                'value' => 'book.title'
                // }
            ],

            'category_id',
            [
                'label' => 'Category Name',
                'attribute' => 'category_id',
                'filter' => Category::find()->select(['name', 'id'])->indexBy('id')->column(),
                // 'value' => function (BookCategory $bookCategory) {
                //     return ArrayHelper::getValue($bookCategory, 'category.name');
                // }
                'value' => 'category.name'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>