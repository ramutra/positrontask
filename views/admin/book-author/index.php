<?php

use app\models\Author;
use app\models\Book;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\admin\search\BookAuthorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Book Authors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-author-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Book Author', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'book_id',
            [
                'label' => 'Book Title',
                'attribute' => 'book_id',
                'filter' => Book::find()->select(['title', 'id'])->indexBy('id')->column(),
                'value' => 'book.title'
            ],

            'author_id',
            [
                'label' => 'Author Name',
                'attribute' => 'author_id',
                'filter' => Author::find()->select(['name', 'id'])->indexBy('id')->column(),
                'value' => 'author.name'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>