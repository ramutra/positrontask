<?php

use app\models\Book;
use app\models\Category;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\admin\search\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Book', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            'id',
            'title:ntext',
            'article',
            // 'page_count',
            'pub_date',
            'pic_url:ntext',
            [
                'label' => 'Categories',
                'attribute' => 'category_id',
                'filter' => Category::find()->select(['name', 'id'])->indexBy('id')->column(),
                'value' => function(Book $book) {
                    return implode(', ', ArrayHelper::map($book->categories, 'id', 'name'));
                }
            ],
            //'short_desc:ntext',
            //'long_desc:ntext',
            // 'status:ntext',
            [
                'attribute' => 'status',
                'filter' => ['PUBLISH' => 'PUBLISH', 'MEAP' => 'MEAP'],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>