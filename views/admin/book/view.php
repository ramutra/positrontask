<?php

use app\models\Author;
use app\models\Category;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Book */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Books', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="book-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= Html::img('@web/' . $model->pic_url, ['alt' => 'aaa']) ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title:ntext',
            'article',
            'page_count',
            'pub_date',
            // 'pic_url:ntext',
            'short_desc:ntext',
            'long_desc:ntext',
            'status:ntext',
        ],
    ]) ?>

    <p>
        <?= Html::a('Add new category', ['admin/book-category/create', 'book_id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider(['query' => $model->getBookCategories()]),
        'columns' => [
            [
                'label' => 'Category Name',
                'attribute' => 'category_id',
                'filter' => Category::find()->select(['name', 'id'])->indexBy('id')->column(),
                'value' => 'category.name'
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => '/admin/book-category',
            ],
        ],
    ]); ?>

    <p>
        <?= Html::a('Add new author', ['admin/book-author/create', 'book_id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider(['query' => $model->getBookAuthors()]),
        'columns' => [
            [
                'label' => 'Author Name',
                'attribute' => 'author_id',
                'filter' => Author::find()->select(['name', 'id'])->indexBy('id')->column(),
                'value' => 'author.name'
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'controller' => '/admin/book-author',
            ],
        ],
    ]); ?>

</div>