<?php

use app\models\Category;
use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\bootstrap\ActiveForm;;;

/* @var $this yii\web\View */
/* @var $model app\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'title')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'page_count')->textInput() ?>

            <?= $form->field($model, 'pub_date')->textInput()->hint('YYYY-MM-DD') ?>

            <?= $form->field($model, 'pic_url')->fileInput() ?>

            <?= $form->field($model, 'short_desc')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'long_desc')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'status')->dropDownList(['MEAP' => 'MEAP', 'PUBLISH' => 'PUBLISH']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'catsArray')->checkboxList(Category::find()->select(['name', 'id'])->indexBy('id')->column()) ?>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>