<?php

use app\models\Author;
use app\models\Category;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\widgets\DetailView;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<?= Html::img('@web/'.$model->pic_url, ['alt' => 'class room']) ?>

<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'title:ntext',
        'article',
        'page_count',
        'pub_date',
        // 'pic_url:ntext',
        'short_desc:ntext',
        'long_desc:ntext',
        'status:ntext',
    ],
]) ?>
    <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider(['query' => $model->getBookCategories()]),
        'columns' => [
            [
                'label' => 'Category Name',
                'attribute' => 'category_id',
                'filter' => Category::find()->select(['name', 'id'])->indexBy('id')->column(),
                'value' => 'category.name'
            ],
        ],
    ]); ?>


    <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider(['query' => $model->getBookAuthors()]),
        'columns' => [
            [
                'label' => 'Author Name',
                'attribute' => 'author_id',
                'filter' => Author::find()->select(['name', 'id'])->indexBy('id')->column(),
                'value' => 'author.name'
            ],
        ],
    ]); ?>