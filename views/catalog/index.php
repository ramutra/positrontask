<?php

use app\models\admin\search\AuthorSearch;
use app\models\Book;
use app\models\Category;
// use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Html;
use app\models\admin\search\BookSearch2;
use app\models\Author;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $searchModel app\models\admin\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Find Your Book';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); 
    ?>
    <table>
        <tr>
            <td>
                <!-- <div class="pull-left"> -->
                <?= GridView::widget([
                    'dataProvider' => $categoriesDataProvider,
                    'filterModel' => $categoriesSearchModel,
                    'columns' => [
                        [
                            'label' => 'Select Category',
                            'attribute' => 'id',

                            'filter' => Category::find()->select(['name', 'id'])->indexBy('id')->column(),
                            'value' => 'name',
                        ],
                    ],
                ]);
                ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php
                $categoriesIds = ArrayHelper::getColumn($categoriesDataProvider->getModels(), 'id');
                $booksSearchModel = new BookSearch2();
                $booksDataProvider = $booksSearchModel->search(Yii::$app->request->queryParams, $categoriesIds);


                echo GridView::widget([
                    'dataProvider' =>  $booksSearchModel->search(Yii::$app->request->queryParams, $categoriesIds),
                    'filterModel' => $booksSearchModel,
                    'columns' => [
                        'id',
                        [
                            'label' => 'Title',
                            'attribute' => 'title',
                            'value' => function (Book $book) {
                                return Html::a(Html::encode($book->title), ['view', 'id' => $book->id]);
                            },
                            'format' => 'raw',
                        ],
                        ['label' => 'Article', 'attribute' => 'article'],
                        ['label' => 'Published Date', 'attribute' => 'pub_date'],
                        [
                            'label' => 'Authors',
                            'attribute' => 'author',
                            // 'filter' => Author ::find()->select(['name', 'id'])->indexBy('id')->column(),
                            'value' => function (Book $book) {
                                return implode(', ', ArrayHelper::getColumn($book->getAuthors()->all(), 'name'));
                            }
                        ],
                        [
                            'label' => 'Status',
                            'attribute' => 'status',
                            'filter' => ['PUBLISH' => 'PUBLISH', 'MEAP' => 'MEAP'],
                        ]
                    ],
                ]);

                ?>
            </td>
        </tr>
    </table>