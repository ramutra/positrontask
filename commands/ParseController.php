<?php

namespace app\commands;

use app\models\Author;
use app\models\Book;
use app\models\BookAuthor;
use app\models\BookCategory;
use app\models\Link;
use app\models\Category;
use Yii;
use yii\console\ExitCode;
use yii\console\Controller;
use yii\helpers\FileHelper;

class ParseController extends Controller
{
    public function actionParse()
    {
        $jsonLinks = ['https://gitlab.com/prog-positron/test-app-vacancy/-/raw/master/books.json'];
        $specialCategories = ['New'];

        foreach ($jsonLinks as $exactLink) {
            $checkLinks = Link::find()->where('rss_link = :rss_link', [':rss_link' => $exactLink])->exists();
            if (!$checkLinks) {
                $link = new Link();
                $link->rss_link = $exactLink;
                $link->save();
            }
        }

        foreach ($specialCategories as $exactCat) {
            $checkCats = Category::find()->where('name = :name', [':name' => $exactCat])->exists();
            if (!$checkCats) {
                $link = new Category();
                $link->name = $exactCat;
                $link->save();
            }
        }

        $exactLink = Link::find()->all();

        foreach ($exactLink as $link) {
            $gottenContent = file_get_contents($link->rss_link);
            $gottenContentArr = json_decode($gottenContent, true);
            $categories = [];
            $authors = [];

            foreach ($gottenContentArr as $book) {
                $objectBook = new Book();
                $objectBook->title = $book['title'];

                if (isset($book['isbn'])) {
                    $articletitleCheck = Book::find()->where('article = :isbn', [':isbn' => $book['isbn']])->andWhere('title = :title', [':title' => $book['title']])->exists();

                    if (!$articletitleCheck) {
                        $objectBook->article = $book['isbn'];
                    } else {
                        continue;
                    }
                } else {
                    $titleCheck = Book::find()->where('title = :title', [':title' => $book['title']])->exists();
                    if (!$titleCheck) {
                        $objectBook->article = uniqid();
                    } else {
                        continue;
                    }
                }

                if (isset($book['pageCount'])) {
                    $objectBook->page_count = $book['pageCount'];
                }

                if (isset($book['publishedDate'])) {
                    foreach ($book as $key => $exact) {
                        if ($key === 'publishedDate') {
                            $objectBook->pub_date = substr($exact['$date'], 0, 10);
                        }
                    }
                }

                if (isset($book['thumbnailUrl'])) {
                    $headers = get_headers($book['thumbnailUrl']);

                    if ($headers[0] == 'HTTP/1.1 200 OK') {
                        $picUrl = $book['thumbnailUrl'];
                        $image = file_get_contents($picUrl);
                        $picName = uniqid();

                        $webDir = Yii::getAlias('@web');

                        foreach ($book as $key => $exact) {

                            if (isset($book['publishedDate'])) {

                                if ($key === 'publishedDate') {
                                    $year = substr($exact['$date'], 0, 4);
                                    $monthAndDay = substr($exact['$date'], 5, 5);
                                    $month = substr($monthAndDay, 0, 2);
                                    $day = substr($monthAndDay, 3, 4);

                                    $dir = sprintf('%s/images/book_cover/%s/%s/%s', $webDir, $year, $month, $day);
                                    $dirPath = sprintf('web/images/book_cover/%s/%s/%s/%s', $year, $month, $day, $picName);
                                    FileHelper::createDirectory($dir, 0777);
                                    file_put_contents($dirPath, $image);
                                    $objectBook->pic_url = sprintf('images/book_cover/%s/%s/%s/%s', $year, $month, $day, $picName);
                                }
                            } else {
                                $noDateImgPath = sprintf('web/images/book_cover/%s', $picName);
                                FileHelper::createDirectory('web/images/book_cover', 0777);
                                file_put_contents($noDateImgPath, $image);
                                $objectBook->pic_url = sprintf('images/book_cover/%s', $picName);
                            }
                        }
                    } else {
                        $objectBook->pic_url = 'clear-img.jpg';
                    }
                } else {
                    $objectBook->pic_url = 'clear-img.jpg';
                }

                if (isset($book['shortDescription'])) {
                    $objectBook->short_desc = $book['shortDescription'];
                }

                if (isset($book['longDescription'])) {
                    $objectBook->long_desc = $book['longDescription'];
                }

                $objectBook->status = $book['status'];
                $objectBook->save();

                if (isset($book['authors'])) {
                    $authors = array_merge($authors, $book['authors']);
                }

                if (isset($book['categories'])) {
                    $book['categories'] = array_map('ucfirst', $book['categories']);
                    $categories = array_merge($categories, $book['categories']);
                }
            }

            ###################<---AUTHORS--->####################
            $authors = array_unique($authors);
            array_map(function ($author) {
                $objectAuthor = new Author();
                $objectAuthor->name = $author;
                $objectAuthor->save();
            }, $authors);

            ###################<---CATEGORIES--->####################
            $categories = array_unique($categories);
            array_map(function ($category) {
                $objectCategory = new Category();
                $objectCategory->name = $category;
                $objectCategory->save();
            }, $categories);

            ###################<---CONNECTION-CREATION--->####################
            foreach ($gottenContentArr as $book) {
                #CATEGORIES#
                if (isset($book['categories']) && !empty($book['categories'])) {
                    $findBookId = Book::find()->select('id')->where('title = :title', [':title' => $book['title']])->one();
                    foreach ($book['categories'] as $Catkey => $exactCategory) {
                        if (isset($book['categories'][$Catkey]) && !empty($book['categories'][$Catkey])) {
                            $findCatId = Category::find()->select('id')->where('name = :cat', [':cat' => $book['categories'][$Catkey]])->one();
                            $objectBookCategory = new BookCategory();
                            $objectBookCategory->book_id = $findBookId->id;
                            $objectBookCategory->category_id = $findCatId->id;
                            $objectBookCategory->save();
                        }
                    }
                } else {
                    $findBookId = Book::find()->select('id')->where('title = :title', [':title' => $book['title']])->one();
                    $findNewCat = Category::find()->select('id')->where('name = :name', [':name' => 'New'])->one();
                    $objectBookCategory = new BookCategory();
                    $objectBookCategory->book_id = $findBookId->id;
                    $objectBookCategory->category_id = $findNewCat->id;
                    $objectBookCategory->save();
                }

                #AUTHORS#
                if (isset($book['authors']) && !empty($book['authors'])) {
                    $findBookId = Book::find()->select('id')->where('title = :title', [':title' => $book['title']])->one();
                    foreach ($book['authors'] as $Authkey => $exactAuthor) {
                        if (isset($book['authors'][$Authkey]) && !empty($book['authors'][$Authkey])) {
                            $findAuthorId = Author::find()->select('id')->where('name = :auth', [':auth' => $book['authors'][$Authkey]])->one();
                            $objectBookAuthor = new BookAuthor();
                            $objectBookAuthor->book_id = $findBookId->id;
                            $objectBookAuthor->author_id = $findAuthorId->id;
                            $objectBookAuthor->save();
                        }
                    }
                }
            }
        }

        return ExitCode::OK;
    }
}
