<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string $title
 * @property string|null $article
 * @property int|null $page_count
 * @property string|null $pub_date
 * @property string|null $pic_url
 * @property string|null $short_desc
 * @property string|null $long_desc
 * @property string|null $status
 *
 * @property BookAuthor[] $bookAuthors
 * @property Author[] $authors
 * @property BookCategory[] $bookCategories
 * @property Category[] $categories
 */
class Book extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title', 'short_desc', 'long_desc', 'status'], 'string'],
            [['page_count'], 'integer'],
            [['pub_date'], 'safe'],
            [['catsArray'], 'safe'],
            [['article'], 'string', 'max' => 255],
            [['pic_url'], 'required'],
            [['pic_url'], 'file', 'extensions' => 'jpg, png, gif'],
            [['id', 'title'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'article' => 'Article',
            'page_count' => 'Page Count',
            'pub_date' => 'Published Date',
            'pic_url' => 'Picture',
            'short_desc' => 'Short Description',
            'long_desc' => 'Long Description',
            'status' => 'Status',
            'catsArray' => 'Categories',
        ];
    }

    /**
     * Gets query for [[BookAuthors]].
     *
     * @return \yii\db\ActiveQuery|BookAuthorQuery
     */
    public function getBookAuthors()
    {
        return $this->hasMany(BookAuthor::class, ['book_id' => 'id']);
    }

    /**
     * Gets query for [[Authors]].
     *
     * @return \yii\db\ActiveQuery|AuthorsQuery
     */
    public function getAuthors()
    {
        return $this->hasMany(Author::class, ['id' => 'author_id'])->viaTable('book_author', ['book_id' => 'id']);
    }

    /**
     * Gets query for [[BookCategories]].
     *
     * @return \yii\db\ActiveQuery|BookCategoryQuery
     */
    public function getBookCategories()
    {
        return $this->hasMany(BookCategory::class, ['book_id' => 'id']);
    }

    /**
     * Gets query for [[Categories]].
     *
     * @return \yii\db\ActiveQuery|CategoriesQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])->viaTable('book_category', ['book_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return BookQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookQuery(get_called_class());
    }

    ################SELECT-MULTIPLE-CATEGORIES################
    private $catsArray;

    public function getCatsArray()
    {
        if ($this->catsArray === null) {
            return $this->getCategories()->select('id')->column();
        }
        return $this->catsArray;
    }
    public function setCatsArray($value)
    {
        return $this->catsArray = (array)$value;
    }
    public function afterSave($insert, $changedAttributes)
    {
        $this->updateCats();
        parent::afterSave($insert, $changedAttributes);
    }
    public function updateCats()
    {
        $currentCatsIds = $this->getCategories()->select('id')->column();
        $newCatsIds = $this->getCatsArray();

        foreach (array_filter(array_diff($newCatsIds, $currentCatsIds)) as $catId) {
            if ($cat = Category::findOne($catId)) {
                $this->link('categories', $cat);
            }
        }
        // foreach (array_filter(array_diff($currentCatsIds, $newCatsIds)) as $catId) {
        //     if ($cat = Category::findOne($catId)) {
        //         $this->unlink('categories', $cat, true);
        //     }
        // }
    }
}
