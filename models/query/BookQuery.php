<?php

namespace app\models\query;

use app\models\BookCategory;

/**
 * This is the ActiveQuery class for [[\app\models\Book]].
 *
 * @see \app\models\Book
 */
class BookQuery extends \yii\db\ActiveQuery
{
    /*public function published()
    {
        return $this->andWhere(['status' => 'PUBLISH']);
    }
    */

    /**
     * {@inheritdoc}
     * @return \app\models\Book[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Book|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function forCat($id)
    {
        return $this->joinWith(['bookCategory'], false)->andWhere([BookCategory::tableName() . '.category_id' => $id]);
    }
}
